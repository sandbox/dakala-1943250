<?php

/**
 * @file
 * Contains \Drupal\site_dump\Form\ConfigExportForm.
 */

namespace Drupal\site_dump\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the configuration export form.
 */
class SiteDumpExportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_dump_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = array(
      '#markup' => '<p>' . $this->t('Use the export button below to download your site configuration.') . '</p>',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state['redirect_route']['route_name'] = 'config.export_download';
  }

  /**
   * Array of exportable components.
   * @return array
   */
  function site_dump_get_components() {
    $components = array(
      'term' => $this->t('Taxonomy terms'),
      'user' => $this->t('Users'),
    );
    foreach (node_type_get_names() as $node_type => $name) {
      $components['node-' . $node_type] = '(node) ' . $name;
    }
    return $components;
  }

}
